﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class CustomTestException : Exception
    {
        public CustomTestException(string message) : base(message)
        {
        }

        public CustomTestException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
