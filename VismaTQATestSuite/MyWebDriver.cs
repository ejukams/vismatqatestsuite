﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;

namespace VismaTQATestSuite
{
    public class MyWebDriver
    {
        public static IWebDriver driver { get; set; }

        public static IWebElement getElement(String locator)
        {
            // vajag pielikt wait uz elementiem, jo var ielādēties vēlāk, pagaidām nav ....
            try
            {
                if (locator.Contains("xpath=") && locator.StartsWith("xpath="))
                {
                    return driver.FindElement(By.XPath(locator.Substring(6)));
                }
                else if (locator.Contains("id=") && locator.StartsWith("id="))
                {
                    return driver.FindElement(By.Id(locator.Substring(3)));
                }
                else if (locator.Contains("css=") && locator.StartsWith("css="))
                {
                    return driver.FindElement(By.CssSelector(locator.Substring(4)));
                }
                else if (locator.Contains("class=") && locator.StartsWith("class="))
                {
                    return driver.FindElement(By.CssSelector(locator.Substring(4)));
                }
                else if (locator.Contains("name=") && locator.StartsWith("name="))
                {
                    return driver.FindElement(By.Name(locator.Substring(5)));
                }
                else
                {
                    fail(String.Format("Norādīts nepareizs lokators {0}, pieejamās " +
                        "vērtības ['xpath=', 'id=', 'css=', 'class=', 'name=']", locator));
                }
            }
            catch (NoSuchElementException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                fail(String.Format("Kļūda meklējot web elementu pēc lokatora {0}", locator), ex);
            }
            return null;
        }

        public static void setValue(String locator, String value)
        {
            IWebElement webElement = getElement(locator);
            if (webElement != null)
                webElement.SendKeys(value);
        }

        public static String getAttribute(String locator, String attrName)
        {
            IWebElement webElement = getElement(locator);
            if (webElement != null)
                return webElement.GetAttribute(attrName);
            else
                return null;
        }

        public static String getText(String locator)
        {
            IWebElement webElement = getElement(locator);
            if (webElement != null)
                return webElement.Text;
            else
                return null;
        }

        public static void clear(String locator)
        {
            IWebElement webElement = getElement(locator);
            if (webElement != null)
                webElement.Clear();
        }

        public static void scrollDownToElement(String locator)
        {
            IWebElement element = getElement(locator);
            //String jsStr = String.Format("window.scrollTo({0}, {1});", element.Location.X, element.Location.Y);
            String jsStr = String.Format("var element = arguments[0]; element.scrollIntoView(false);");
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(jsStr, element);
        }

        public static int getWindowHeigth()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor) driver;
            return (int)(long)js.ExecuteScript("return window.innerHeight"); 
        }

        public static void click(String locator)
        {
            IWebElement element = getElement(locator);
            element.Click();
        }

        public static String getURL()
        {
            return driver.Url;
        }
    }
}
