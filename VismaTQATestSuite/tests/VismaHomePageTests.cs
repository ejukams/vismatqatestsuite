﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class VismaHomePageTests : TestBase
    {
        [Test(Description = "Check if Visma home page was opened, logo present and close")]
        public void openCloseVismaHomePage()
        {
            VismaHomePagePOM.openPage(VismaHomePagePOM.pageURL);
            VismaHomePagePOM.Clickables.logo.verifyPresent();
            VismaHomePagePOM.Clickables.logo.click();
            VismaHomePagePOM.closePage();
        }

        [Test(Description = "Open Visma home page, open presentation request form, check if input present and close")]
        public void openVismaPresentationRequestFormPage()
        {
            VismaHomePagePOM.openPage(VismaHomePagePOM.pageURL);
            VismaHomePagePOM.Clickables.logo.verifyPresent();
            VismaHomePagePOM.Clickables.pieteiktPrezentaciju.click();

            VismaPresentationReqFormPOM.Inputs.vards.verifyPresent();
            VismaPresentationReqFormPOM.closePage();

        }
    }
}
