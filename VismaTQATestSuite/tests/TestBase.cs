﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    [TestFixture]
    public class TestBase
    {
        //[OneTimeSetUp]
        [SetUp]
        public void initDriver()
        {
            MyWebDriver.driver = new ChromeDriver();
            MyWebDriver.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            // šeit jāpieliek no config faila vai no env variables drivera tipa (firefox, opera, ie.... un citu parametru inicializācija
            // TO DO
        }

        //[OneTimeTearDown]
        [TearDown]
        public void tearDownDriver()
        {
            MyWebDriver.driver.Quit();
        }

        public static void fail(String info)
        {
            throw new CustomTestException(info);
        }

        public static void fail(String info, Exception ex)
        {
            throw new CustomTestException(info, ex);
        }
    }

}
