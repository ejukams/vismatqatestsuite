﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;

namespace VismaTQATestSuite
{
    class VismaPresentationReqFormTests : TestBase
    {
        [Test(Description = "Check if all requested INPUT fields are mandatory")]
        public void checkVismaPresentationRequestForm()
        {
            VismaHomePagePOM.openPage(VismaHomePagePOM.pageURL);
            VismaHomePagePOM.Clickables.logo.verifyPresent();
            VismaHomePagePOM.Clickables.pieteiktPrezentaciju.click();

            VismaPresentationReqFormPOM.Clickables.pieteikties.click();
            VismaPresentationReqFormPOM.Inputs.vards
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Šis lauks ir obligāts.");

            VismaPresentationReqFormPOM.Inputs.uzvards
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Šis lauks ir obligāts.");

            VismaPresentationReqFormPOM.Inputs.organizacija
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Šis lauks ir obligāts.");

            VismaPresentationReqFormPOM.Inputs.talruna_nr
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Šis lauks ir obligāts.");

            VismaPresentationReqFormPOM.Inputs.epasts
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Šis lauks ir obligāts.");

            VismaPresentationReqFormPOM.Inputs.vards.setValue("Edgars");
            VismaPresentationReqFormPOM.Inputs.uzvards.setValue("Jukāms");
            VismaPresentationReqFormPOM.Inputs.organizacija.setValue("Visma Labs");
            VismaPresentationReqFormPOM.Inputs.talruna_nr.setValue("1234567");
            VismaPresentationReqFormPOM.Inputs.epasts.setValue("test@test.lv");

            VismaPresentationReqFormPOM.Clickables.pieteikties.click();

            VismaPresentationReqFormPOM.Inputs.vards
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();

            VismaPresentationReqFormPOM.Inputs.uzvards
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();

            VismaPresentationReqFormPOM.Inputs.organizacija
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();

            VismaPresentationReqFormPOM.Inputs.talruna_nr
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();

            VismaPresentationReqFormPOM.Inputs.epasts
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();
        }

        [Test(Description = "Verify email format")]
        public void verifyVismaPresentationRequestFormEmailFormat()
        {
            checkVismaPresentationRequestForm();
            List<String> emails = new List<string>();
            emails.Add("test@test.com");
            emails.Add("test@test.");
            emails.Add("test@test.abc.com");
            emails.Add("test@@test.com");
            foreach (String email in emails)
            {
                VismaPresentationReqFormPOM.Inputs.epasts.clearValue();
                VismaPresentationReqFormPOM.Inputs.epasts.setValue(email);
                VismaPresentationReqFormPOM.Clickables.pieteikties.click();
                // regex no šejienes https://emailregex.com/
                if (Regex.Match(email, @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$").Success)
                {
                    VismaPresentationReqFormPOM.Inputs.epasts.verifyErrorMessageNotVisible();
                }
                else
                {
                    VismaPresentationReqFormPOM.Inputs.epasts.verifyErrorMessageVisible()
                            .verifyErrorMessageEquals("Ievadiet derîgu epasta adresi.");
                }
            }    
        }

        [Test(Description = "Verify that mandatory checkbox should be pressed before submitting the request ")]
        public void verifyMandatoryCheckBoxBeforeSubmit()
        {
            checkVismaPresentationRequestForm();
            VismaPresentationReqFormPOM.Clickables.obligataIzvelnesRuts
                .verifyErrorMessagePresent()
                .verifyErrorMessageVisible()
                .verifyErrorMessageEquals("Mums ir nepieciešama Jūsu piekrišana Šis lauks ir obligāts.");
            VismaPresentationReqFormPOM.Clickables.obligataIzvelnesRuts.click();
            VismaPresentationReqFormPOM.Inputs.epasts.clearValue();
            VismaPresentationReqFormPOM.Clickables.pieteikties.click();
            VismaPresentationReqFormPOM.Clickables.obligataIzvelnesRuts
                .verifyErrorMessagePresent()
                .verifyErrorMessageNotVisible();
        }

        [Test(Description = "Return back to the main page, scroll down and check that blog links opens new tab with different blog records. ")]
        public void returnAndCheckBlog()
        {
            verifyMandatoryCheckBoxBeforeSubmit();
            VismaPresentationReqFormPOM.goBack();
            VismaHomePagePOM.scrollDownToElement(VismaHomePagePOM.Clickables.blogaAktivaisLinks);
            VismaHomePagePOM.Clickables.blogaAktivaisLinks.click();
            VismaHomePagePOM.switchToLastOpenedTab();
            VismaBlogPOM.verifyBodyContainsText("Visma Blog Latvia");
        }

        [Test(Description = "Check if social links opens correctly")]
        public void checkIfSocialLinksOpenCorrectly()
        {
            // vispar jau vajadzeja ar http request linku parbaudit vai atgriez 200 statusu :(
            VismaHomePagePOM.openPage(VismaHomePagePOM.pageURL);
            VismaHomePagePOM.scrollDownToElement(VismaHomePagePOM.Clickables.facebook_soc);
            VismaHomePagePOM.Clickables.facebook_soc.click();
            VismaHomePagePOM.switchToLastOpenedTab();
            VismaHomePagePOM.verifyContainsElement("xpath=//a[contains(@href, \"facebook.com/VISMALatvia\")]");
            VismaHomePagePOM.switchToFirstTab();
            VismaHomePagePOM.scrollDownToElement(VismaHomePagePOM.Clickables.linkedin_soc);
            VismaHomePagePOM.Clickables.linkedin_soc.click();
            VismaHomePagePOM.switchToLastOpenedTab();
            VismaHomePagePOM.verifyBodyContainsText("Visma Latvia");
            VismaHomePagePOM.switchToFirstTab();
            VismaHomePagePOM.scrollDownToElement(VismaHomePagePOM.Clickables.blog_soc);
            VismaHomePagePOM.Clickables.blog_soc.click();
            VismaHomePagePOM.switchToLastOpenedTab();
            VismaHomePagePOM.verifyBodyContainsText("Visma Blog Latvia");


        }

        [Test(Description = "Check if new tab opens and language changes i")]
        public void checkLanguagChange()
        {
            VismaHomePagePOM.openPage(VismaHomePagePOM.pageURL);
            VismaHomePagePOM.scrollDownToElement(VismaHomePagePOM.Selectables.country_language);
            String lang = VismaHomePagePOM.getPageLanguage();
            String url = VismaHomePagePOM.getPageUrl();
            Console.WriteLine("lang: {0}, url: {1}", lang, url);
            VismaHomePagePOM.Selectables.country_language.select("Finland");
            if (lang.Equals(VismaHomePagePOM.getPageLanguage()))
                fail("Nenomainās valoda");
            if (url.Equals(VismaHomePagePOM.getPageUrl()))
                fail("Nenomainās url");
            Console.WriteLine("lang: {0}, url: {1}", VismaHomePagePOM.getPageLanguage(), VismaHomePagePOM.getPageUrl());

        }
    }
}
