﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class ClickableWithError : ErrorableBaseElement
    {
        public ClickableWithError(String locator, String errorMessageLocator)
        {
            this.locator = locator;
            this.errorMessageLocator = errorMessageLocator;
        }
        public ClickableWithError click()
        {
            MyWebDriver.click(locator);
            return this;
        }
    }
}
