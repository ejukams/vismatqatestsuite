﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.MyWebDriver;
using static VismaTQATestSuite.TestBase;

namespace VismaTQATestSuite
{
    class InputWithError : ErrorableBaseElement
    {
        public InputWithError(String name, String locator, String errorMessageLocator)
        {
            this.name = name;
            this.locator = locator;
            this.errorMessageLocator = errorMessageLocator;
        }

        public InputWithError setValue(string str)
        {
            if (getElement(locator) != null)
                MyWebDriver.setValue(locator, str);
            else
                fail(String.Format("Nevar atrast lauku \"{0}\"", name));
            return this;
        }
        
        public InputWithError clearValue() {
            clear(locator);
            return this;
        }
    }
}
