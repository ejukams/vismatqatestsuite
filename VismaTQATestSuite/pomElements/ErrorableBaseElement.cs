﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;
using static VismaTQATestSuite.MyWebDriver;

namespace VismaTQATestSuite
{
    abstract class ErrorableBaseElement : BaseElement
    {
        protected String errorMessageLocator { get; set; }

        public ErrorableBaseElement verifyErrorMessagePresent()
        {
            if (getElement(errorMessageLocator) == null)
                fail(String.Format("Laukam \"{0}\" nevar atrast kļūdu paziņojumu", name));
            return this;
        }
        public ErrorableBaseElement verifyErrorMessageVisible()
        {
            verifyErrorMessagePresent();
            if (!getElement(errorMessageLocator).Displayed)
                fail(String.Format("Laukam \"{0}\" nav redzams kļūdu paziņojums", name));
            return this;
        }
        public ErrorableBaseElement verifyErrorMessageNotPresent()
        {
            if (getElement(errorMessageLocator) != null)
                fail(String.Format("Laukam \"{0}\" var atrast kļūdu paziņojumu", name));
            return this;
        }
        public ErrorableBaseElement verifyErrorMessageNotVisible()
        {
            verifyErrorMessagePresent();
            if (getElement(errorMessageLocator).Displayed)
                fail(String.Format("Laukam \"{0}\" ir redzams kļūdu paziņojums", name));
            return this;
        }

        public ErrorableBaseElement verifyErrorMessageEquals(String message)
        {
            verifyErrorMessagePresent();
            String textPresent = getText(errorMessageLocator);
            if (!textPresent.Equals(message))
                fail(String.Format("Laukam \"{0}\" kļūdu paziņojums nav \"{1}\", bet ir \"{2}\"", name, message, textPresent));
            return this;
        }

        public ErrorableBaseElement verifyErrorMessageNotEquals(String message)
        {
            verifyErrorMessagePresent();
            String textPresent = getText(errorMessageLocator);
            if (textPresent.Equals(message))
                fail(String.Format("Laukam \"{0}\" kļūdu paziņojums ir \"{1}\"", name, textPresent));
            return this;
        }
    }
}
