﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class Clickable : BaseElement
    {
        public Clickable(String locator)
        {
            this.locator = locator;
        }
        public Clickable click()
        {
            MyWebDriver.click(locator);
            return this;
        }

        


    }
}
