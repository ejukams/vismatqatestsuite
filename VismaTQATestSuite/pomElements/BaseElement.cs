﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;

namespace VismaTQATestSuite
{
    abstract class BaseElement : IElement
    {
        public string name { get; set; }
        public string locator { get; set; }

        public BaseElement verifyPresent()
        {
            if (MyWebDriver.getElement(locator) == null)
                fail(String.Format("Elements {0} nav atrodams", name));
            return this;
        }
    }
}
