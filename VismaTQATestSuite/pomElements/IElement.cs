﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    interface IElement
    {
        String name { get; set; }
        String locator { get; set; }

    }
}
