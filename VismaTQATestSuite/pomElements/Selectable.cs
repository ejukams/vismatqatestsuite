﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;

namespace VismaTQATestSuite
{
    class Selectable : BaseElement
    {
        private List<String> selectors;

        public Selectable(String locator, String name, List<String> selectors)
        {
            this.locator = locator;
            this.name = name;
            this.selectors = selectors;
        }

        public Selectable select(String value)
        {
            if (!selectors.Contains(value))
                fail(String.Format("Selektējamam elementam {0} nav definēts selektors {1}", name, value));

            IWebElement element = MyWebDriver.getElement(locator);
            if (element != null)
                element.Click();
            else
                fail(String.Format("Nevar atrast elementu ar lokatoru {0}", locator));

            element = MyWebDriver.getElement("xpath=//li[@class='select2-results__option' and @title='" + value + "']/span/img");
            if (element != null)
                element.Click();
            else
                fail(String.Format("Nevar atrast elementu ar selektoru {0}", value));
            return this;
        }
    }
}
