﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class VismaPresentationReqFormPOM : BasePOM
    {
        public static string pageURL = "https://www.visma.lv/visma-par-mums/prezentacijas-pieteikums/";

        public static class Inputs
        {
            public static InputWithError vards = new InputWithError("Vārds", "id=d5bc44f9-b450-4cf3-9793-4feebd63d8e5", "xpath=//input[@id='d5bc44f9-b450-4cf3-9793-4feebd63d8e5']/following-sibling::span[@class='Form__Element__ValidationError']");
            public static InputWithError uzvards = new InputWithError("Uzvārds", "id=f07bac8f-1d6e-4ce4-96a5-4c8b833dad99", "xpath=//input[@id='f07bac8f-1d6e-4ce4-96a5-4c8b833dad99']/following-sibling::span[@class='Form__Element__ValidationError']");
            public static InputWithError organizacija = new InputWithError("Organizācija", "id=0ae753ec-257e-42c1-8f66-968460d583e8", "xpath=//input[@id='0ae753ec-257e-42c1-8f66-968460d583e8']/following-sibling::span[@class='Form__Element__ValidationError']");
            public static InputWithError talruna_nr = new InputWithError("Tālruņa nr.", "id=7af8d3d6-16b6-4921-8a84-00774595ddb3", "xpath=//input[@id='7af8d3d6-16b6-4921-8a84-00774595ddb3']/following-sibling::span[@class='Form__Element__ValidationError']");
            public static InputWithError epasts = new InputWithError("E-pasts", "id=3145b073-19fa-4912-97b7-a850e6900421", "xpath=//input[@id='3145b073-19fa-4912-97b7-a850e6900421']/following-sibling::span[@class='Form__Element__ValidationError']");
        }

        public static class Clickables
        {
            public static Clickable pieteikties = new Clickable("name=submit");
            public static ClickableWithError obligataIzvelnesRuts = new ClickableWithError("id=311b75c7-650b-4977-8a81-3524032e8081_profiling", "xpath=//input[@id='311b75c7-650b-4977-8a81-3524032e8081_profiling']/parent::label/following-sibling::span");
        }

    }
}
