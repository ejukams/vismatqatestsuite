﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VismaTQATestSuite
{
    class VismaHomePagePOM : BasePOM
    {
        public static string pageURL = "http://www.visma.lv";
        public static class Clickables
        {
            public static Clickable logo = new Clickable("xpath=//div[@id='header']/div/div/div[1]/a/img");
            public static Clickable pieteiktPrezentaciju = new Clickable("xpath=//*[@id='frontpagebanner']/div/div/div/div[1]/div/div/p[2]/a");
            public static Clickable blogaAktivaisLinks = new Clickable("xpath=//ul[@class='newsticker']/li[not(contains(@style, 'display: none'))]/a");
            public static Clickable facebook_soc = new Clickable("xpath=//a[contains(text(), 'Facebook')]");
            public static Clickable linkedin_soc = new Clickable("xpath=//a[contains(text(), 'LinkedIn')]");
            public static Clickable blog_soc = new Clickable("xpath=//a[contains(text(), 'Blog')]");
        }

        public static class Selectables {
            public static Selectable country_language = new Selectable("xpath=//span[contains(@id,\"select2-\") and contains(@id, \"-container\")]", "country_language", new List<string>(new String[] { "Latvia", "Denmark", "Finland"}));
        }
    }
}