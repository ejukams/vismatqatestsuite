﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VismaTQATestSuite.TestBase;
using static VismaTQATestSuite.MyWebDriver;

namespace VismaTQATestSuite
{
    class BasePOM
    {
        public static void goBack()
        {
            MyWebDriver.driver.Navigate().Back();
        }

        public static void goForward()
        {
            MyWebDriver.driver.Navigate().Forward();
        }

        public static void refreshPage()
        {
            MyWebDriver.driver.Navigate().Refresh();
        }

        public static void scrollDownToElement(IElement element)
        {
            MyWebDriver.scrollDownToElement(element.locator);
        }

        public static void openPage(String pageURL)
        {
            MyWebDriver.driver.Manage().Window.Maximize();
            MyWebDriver.driver.Navigate().GoToUrl(pageURL);
        }

        public static void closePage()
        {
            MyWebDriver.driver.Close();
        }

        public static void verifyBodyContainsText(String text)
        {
            IWebElement element = MyWebDriver.driver.FindElement(By.XPath("//*[contains(text(), \"" + text + "\")]"));
            if (element == null)
            {
                fail(String.Format("Lapa nesatur tekstu {0}", text));
            }

        }

        public static void switchToLastOpenedTab()
        {
            MyWebDriver.driver.SwitchTo().Window(MyWebDriver.driver.WindowHandles.Last());
        }

        public static void switchToFirstTab()
        {
            MyWebDriver.driver.SwitchTo().Window(MyWebDriver.driver.WindowHandles.First());
        }

        public static String getPageLanguage()
        {
            return MyWebDriver.getAttribute("xpath=//html", "lang");
        }

        public static String getPageUrl()
        {
            return MyWebDriver.getURL();
        }

        public static void verifyContainsElement(String locator)
        {
            IWebElement element = getElement(locator);
            if (element == null)
            {
                fail(String.Format("Lapa nesatur elementu ar lokatoru {0}", locator));
            }
        }
    }
}
